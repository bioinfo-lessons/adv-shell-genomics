echo "Running STAR index..."
STAR \
 --runThreadN 4 \
 --runMode genomeGenerate \
 --genomeDir res/genome/star_index/ \
 --genomeFastaFiles res/genome/ecoli.fasta \
 --genomeSAindexNbases 9

for sid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq)
do
    bash scripts/pipeline.sh $sid
done
