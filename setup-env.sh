conda create -c conda-forge -c bioconda -y -n advshell fastqc seqtk cutadapt star multiqc

export WD=~/adv-shell
mkdir -p $WD
rm -rf $WD/data/*
mkdir -p $WD/{data,log,out,res}

wget -P $WD/data ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR286/002/ERR2868172/ERR2868172_1.fastq.gz
wget -P $WD/data ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR286/002/ERR2868172/ERR2868172_2.fastq.gz

mkdir -p $WD/res/genome
wget -O $WD/res/genome/ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip $WD/res/genome/ecoli.fasta.gz
